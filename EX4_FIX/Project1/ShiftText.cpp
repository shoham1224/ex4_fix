#include "ShiftText.h"

ShiftText::ShiftText(string text, int key) : PlainText(text)
{
	this->_key = key;
	this->_text = encrypt();
	this->_name = "Shift";

}

ShiftText::~ShiftText()
{
}

string ShiftText::encrypt()
{
	
	string enc = _text;
	int i = 0;
	for (i = 0; i < enc.length(); i++)
	{
		if (int(enc[i]) >= int(RESET))
		{
			enc[i] = char(((int(enc[i]) - int(RESET) + _key) % MAX) + int(RESET));
		}
	}
	this->_isEncrypted = true;
	return enc;
}

string ShiftText::decrypt()
{
	string dec = _text;
	int i = 0;
	for (i = 0; i < dec.length(); i++)
	{
		if (int(dec[i]) >= int(RESET))
		{
			if ((int(dec[i]) - int(RESET)) - _key < 0)
			{
				dec[i] = char(int(dec[i]) - _key + MAX);
			}
			else
			{
				dec[i] = char(int(dec[i]) - _key);
			}
		}
	}
	this->_isEncrypted = false;
	return dec;
}
