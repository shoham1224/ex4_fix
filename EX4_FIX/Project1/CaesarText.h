#pragma once
#include "ShiftText.h"

#define KEY 3

class CaesarText : public ShiftText
{
public:
	CaesarText(string text); //shifts the text by 3
	~CaesarText();
};