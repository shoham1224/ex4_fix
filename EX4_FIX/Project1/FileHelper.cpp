#include "FileHelper.h"


string FileHelper::readFileToString(string fileName)
{
	ifstream theFile(fileName);
	string curr = "";
	string rval = "";

	while (getline(theFile, curr))
	{
		rval += curr;
	}
	theFile.close();
	return rval;
}

void FileHelper::writeWordsToFile(string inputFileName, string outputFileName)
{
	ofstream outFile(outputFileName);
	ifstream inputFile(inputFileName);
	string word = "";
	while (inputFile >> word)
	{
		outFile << word;
	}
	inputFile.close();
	outFile.close();
}			
			