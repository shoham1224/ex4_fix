#include "PlainText.h"

int PlainText::counter = 0;

PlainText::PlainText(string text)
{
	this->_text = text;
	this->_isEncrypted = false;
	this->_name = "Plain";
	counter++;
}

PlainText::~PlainText()
{
}

bool PlainText::isEnc()
{
	return _isEncrypted;
}

string PlainText::getText()
{
	return this->_text;
}

ostream& operator<<(ostream& output, const PlainText& text)
{
	output << text._name << "\n" << text._text;
	return output;
}
