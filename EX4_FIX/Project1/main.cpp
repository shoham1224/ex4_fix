#include "ShiftText.h"
#include "SubstitutionText.h"
#include "CaesarText.h"
#include "FileHelper.h"

#define SUB_DICTIONARY_FILE_NAME "dictionary.csv"
#define EXIT 4
#define BAD_RES 'A'
#define BAD_MAX 'Z'

//example.txt wont work because it contains a big letter at the start of it, and in the instructions we were supposed to not deal with it. if it is neccery to change and just skip those letter, just put "return true" at the start of the "isGood" function.

int menu();//prints the initial menu
void ciphStr();//encrypts+decrypts a string from an input
void ciphFile(); //encrypts+decrypts a string in a file
int cnt(); //prints how many times the class was called
int getCiph(); //prints and gets the way to cipher
string getText(); //gets a good text
bool isGood(string check);//checks if the text has no big letters

int main()
{
	int option = 0;
	while (option != EXIT)
	{
		option = menu();
		switch (option)
		{
		case 1:
			ciphStr();
			break;
		case 2:
			ciphFile();
			break;
		case 3:
			cout << "the text has been called " << cnt() << " times by now(remeber that it was called twice each time, for decryption and encryption)" << endl;
			break;
		default:
			cout << "thank you for using shoham's cypher text!" << endl;
			break;
		}
	}

	return 0;
}

int menu()
{
	int option = 0;
	do
	{
		cout << "\n\n\nplease enter an option between 1 and 4:\n1.encrypt/decrypt string\n2.encrypt/decrypt file\n3.number of texts\n4.exit\n" << endl;
		cin >> option;

	} while (option<=0 || option> EXIT);
	return option;
}

void ciphStr()
{
	string text = getText();
	int key = 0;
	switch (getCiph())
	{
	case 1:
		cout << "please enter your key: " << endl;
		cin >> key;
		cout << "your new encrypted text is: " << ShiftText(text, key) << endl;
		cout << "your new decrypted text is: " << ShiftText(text, key).decrypt() << endl;
		break;
	case 2:
		cout << "your new encrypted text is: " << CaesarText(text) << endl;
		cout << "your new decrypted text is: " << CaesarText(text).decrypt() << endl;
		break;
	default:
	{
		cout << "your new encrypted text is: " << SubstitutionText(text, SUB_DICTIONARY_FILE_NAME) << endl;
		cout << "your new decrypted text is: " << SubstitutionText(text, SUB_DICTIONARY_FILE_NAME).decrypt() << endl;
		break;
	}
	}
}

void ciphFile()
{
	string fileName = "";
	string text = "";
	string decText = "";
	int option = 0;
	int key;

	do
	{
		cout << "please enter the file name you would like to encrypt/decrypt: " << endl;
		cin >> fileName;
		text = FileHelper().readFileToString(fileName);
		decText = text;
	} while (!isGood(text));
	switch (getCiph())
	{
	case 1:
		cout << "please enter your key: " << endl;
		cin >> key;
		text = ShiftText(text, key).getText();
		decText = ShiftText(decText, key).decrypt();
		break;
	case 2:
		text = CaesarText(text).getText();
		decText = CaesarText(decText).decrypt();
		break;
	default:
		text = SubstitutionText(text, SUB_DICTIONARY_FILE_NAME).getText();
		decText = SubstitutionText(decText, SUB_DICTIONARY_FILE_NAME).encrypt();
		break;
	}
	do
	{
		cout << "would you like to:\n1.save the file onto your coumputer\n2.print the output on the screen?\n";
		cin >> option;
	} while (option <= 0 || option > 2);
	if (option == 1)
	{
		cout << "enter the new files name(without the txt): " << endl;
		cin >> fileName;
		ofstream thisFile(fileName + "_encrypted.txt");
		thisFile << text;
		thisFile.close();
		ofstream thisDecFile(fileName + "_decrypted.txt");
		thisDecFile << decText;
		thisDecFile.close();
	}
	else
	{
		cout << "your new encrypted text is: " << text << endl;
		cout << "your new decrypted text is: " << decText << endl;
	}
}

int cnt()
{
	return --PlainText("").counter;
}

int getCiph()
{
	int option = 0;
	do
	{
		cout << "please choose a way to cypher:\n1.shift\n2.caesar\n3.substitution\n" << endl;
		cin >> option;
	} while (option <= 0 || option >= EXIT);
	return option;
}

string getText()
{
	string rval = "";
	do
	{
		cout << "please enter your string: " << endl;
		cin >> rval;

	} while (!isGood(rval));
	return rval;
}

bool isGood(string check)
{
	bool rval = true;
	int i = 0;
	for (i = 0; i < check.length() && rval; i++)
	{
		if ((int(BAD_RES) < int(check[i])) && (int(check[i]) < int(BAD_MAX)))
		{
			rval = false;
		}
	}
	return rval;

}