#pragma once
#include "PlainText.h"
#include <fstream>

#define VAL 2

class SubstitutionText : public PlainText
{
public:
	SubstitutionText(string text, string dictionaryFileName);
	~SubstitutionText();
	string encrypt();
	string decrypt();
};