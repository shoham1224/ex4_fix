#pragma once

#include<iostream>
#include<string>

#define RESET 'a'

using namespace std;

class PlainText
{
public:
	PlainText(string text);//sets _text to the text, _name to "plaintext", _isEncrypted to false
	~PlainText();
	bool isEnc();//returns isEnc
	string getText();//returns the text
	static int counter;
	friend ostream& operator<<(ostream& output, const PlainText& text);//converts the cout
protected:
	string _name;
	string _text;
	bool _isEncrypted;
};