#pragma once

#include "PlainText.h"

#define MAX 26

class ShiftText : public PlainText
{
public:
	ShiftText(string text, int key);//sets the _name to "ShiftText", sets the _key to the given key, encrypts the text
	~ShiftText();
	string encrypt();//shifts the text according to key, sets isEncrypted to true
	string decrypt();//shifts the text according to -key, sets isEncrypted to true

protected:
	int _key;
};