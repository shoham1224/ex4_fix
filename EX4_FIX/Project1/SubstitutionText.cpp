#include "SubstitutionText.h"

SubstitutionText::SubstitutionText(string text, string dictionaryFileName) : PlainText(text)
{
	this->_text = encrypt();
	this->_name = "Subtitution";
}

SubstitutionText::~SubstitutionText()
{
}

string SubstitutionText::encrypt()
{
	string curr = "";
	string rval = "";
	ifstream theFile("dictionary.csv");
	int i = 0;
	bool done = false;
	for (i = 0; i < _text.length(); i++)
	{
		theFile.seekg(0);
		done = false;
		if (int(_text[i]) >= int(RESET))
		{
			while (getline(theFile, curr) && !done)
			{
				if (curr[0] == _text[i])
				{
					rval += curr[VAL];
					done = true;
				}
			}
		}
		else
		{
			rval += _text[i];
		}
	}
	this->_isEncrypted = true;
	theFile.close();
	return rval;
}

string SubstitutionText::decrypt()
{
	string curr = "";
	string rval = "";
	ifstream theFile("dictionary.csv");
	int i = 0;
	bool done = false;
	for (i = 0; i < _text.length(); i++)
	{
		theFile.seekg(0);
		done = false;
		if (int(_text[i]) >= int(RESET))
		{
			while (getline(theFile, curr) && !done)
			{
				if (curr[VAL] == _text[i])
				{
					rval += curr[0];
					done = true;
				}
			}
		}
		else
		{
			rval += _text[i];
		}
	}
	theFile.close();
	this->_isEncrypted = false;
	return rval;
}
