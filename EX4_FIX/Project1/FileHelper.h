#pragma once
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class FileHelper
{
public:
	static string readFileToString(string fileName); //converts file to string
	static void writeWordsToFile(string inputFileName, string outputFileName);  //copys a file to another
};